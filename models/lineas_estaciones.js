'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Lineas_Estaciones extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Lineas_Estaciones.init({
    idEstacion: DataTypes.INTEGER,
    origen: DataTypes.STRING,
    destino: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Lineas_Estaciones',
  });
  return Lineas_Estaciones;
};