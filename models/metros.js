'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Metros extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Metros.init({
    idLinea: DataTypes.INTEGER,
    vagones: DataTypes.INTEGER,
    capacidad: DataTypes.INTEGER,
    modelo: DataTypes.STRING,
    asientos: DataTypes.INTEGER,
    tipo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Metros',
  });
  return Metros;
};