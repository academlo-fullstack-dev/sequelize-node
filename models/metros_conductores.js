'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Metros_Conductores extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Metros_Conductores.init({
    idConductor: DataTypes.INTEGER,
    turno: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Metros_Conductores',
  });
  return Metros_Conductores;
};