'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Lineas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Lineas.init({
    nombre: DataTypes.STRING,
    color: DataTypes.STRING,
    horaPartida: DataTypes.DATE,
    horaLlegada: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Lineas',
  });
  return Lineas;
};